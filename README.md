# Epubfy 

Flask server that creates epub books out of blog articles.

### Features:
* Detects blog metadata system and extracts it (under dev)
* Detects many different article structures (mostly always finding the content)
* Modern and dynamic user interface

## Deploying

Epubfy was created using Flask + Bootstrap SASS. No Javascript on the front-end.
NPM and webpack are used to generate the static resources.

### Locally

You should use a virtualenv for this.

```sh
git clone https://github.com/morato/epubfy.git
cd epubfy
pip install -r requirements.txt
pip install honcho
npm install
npm build
```
Set up your DB before starting.
```
export DATABASE_URL=postgresql://user:password@localhost:5432/dbname
honcho start -p 8000
```

### Docker

```sh
git clone https://github.com/morato/epubfy.git
cd epubfy
pip install -r requirements.txt
npm install
npm build
docker-compose up
```

### Other

Be sure to set PUBLIC_PATH environment variable so that webpack knows how to link your assets properly. It should be in the format:

```sh
export PUBLIC_PATH="http://localhost:8000/static/"
```

And the DB path:

```sh
export DATABASE_URL=postgresql://user:password@localhost:5432/dbname
```

## References

Below follows some bibliography and references on epub and metadata.

This implements the whole parser:

* https://mercury.postlight.com/web-parser/

### Calibre

* http://www.okyay4.com/nevsblog/2012/accessing-calibre-database-with-python/
* https://www.digitalocean.com/community/tutorials/how-to-create-a-calibre-ebook-server-on-ubuntu-14-04
* https://github.com/Fmstrat/spcs


### Joomla

* https://docs.joomla.org/Microdata
* https://www.w3.org/2001/sw/wiki/Goodrelations_for_Joomla
* http://extensions.joomla.org/extensions/extension/authoring-a-content/blog-integration/wordpress-blog-for-joomla


### HTML

* https://en.wikipedia.org/wiki/Pingback
* https://html5.validator.nu/
* https://www.w3.org/TR/microdata/


## Microformats, microdata, ogp, RDFa, Linked data, JSON-LD

### Extraction/validation
* https://waterpigs.co.uk/php-mf2/
* https://developers.google.com/structured-data/testing-tool/

* http://stackoverflow.com/questions/3325864/what-support-does-html5-provide-for-semantic-markup-and-semantic-web-technologie/27175699#27175699

### OGP
* http://itprism.com/blog/19-opengraph-microformats-semantic

* https://wiki.mozilla.org/Microformats
* http://stackoverflow.com/questions/6402528/opengraph-or-schema-org
* http://webmasters.stackexchange.com/questions/2860/microdata-vs-rfda/53140#53140
* http://programmers.stackexchange.com/questions/166612/schema-org-vs-microformats/166669#166669
* http://stackoverflow.com/questions/8957902/microdata-vs-rdfa/25888436#25888436
* http://blog.foolip.org/2009/08/23/microformats-vs-rdfa-vs-microdata/
* http://manu.sporny.org/2011/uber-comparison-rdfa-md-uf/

* http://microformats.org/wiki/parsers
* http://microformats.org/wiki/mf2py
* http://microformats.org/wiki/code-tools
* http://microformats.org/wiki/h-entry
* http://microformats.org/wiki/microformats-2

* https://www.npmjs.com/package/suq

* https://www.w3.org/TR/json-ld/

## Epub

* https://github.com/mattharrison/epub-css-starter-kit
* https://github.com/javierarce/epub-boilerplate
* https://github.com/reitermarkus/epub3-boilerplate
* https://github.com/eCrowdMedia/ebooklib/blob/master/ebooklib/epub.py
* https://github.com/eszkadev/epub-creator/blob/master/epubcreator.py
* https://github.com/meng89/epubuilder/blob/master/epubuilder/epubuilder.py
* https://github.com/setanta/ebookmaker/blob/master/ebookmaker.py
* https://github.com/dpapathanasiou/CleanScrape/blob/master/CleanScraper.py
* https://github.com/macborow/repub/
