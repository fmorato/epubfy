FROM python:3.5
ENV PYTHONUNBUFFERED 1

CMD mkdir -p /app
WORKDIR /app

COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY . /app
RUN pip install -e /app

EXPOSE 8000
ENV PORT 8000
ENV APP_SETTINGS config.ProductionConfig

CMD ["gunicorn", "epubfy:app"]
