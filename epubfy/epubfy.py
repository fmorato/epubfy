import os

import errno
from flask import Flask
from flask_babel import Babel
from flask_compress import Compress
from flask_htmlmin import HTMLMIN
from flask_webpack import Webpack
from flask_tryton import Tryton

app = Flask(
    __name__,
    static_folder=os.path.join(os.path.dirname(os.path.abspath(__file__)), "static", "assets"),
    static_url_path="/static"
)

app.config.from_object(os.environ['APP_SETTINGS'])
app.config['MINIFY_PAGE'] = True

babel = Babel(app)

Compress(app)
HTMLMIN(app)

app.config['WEBPACK_MANIFEST_PATH'] = WEBPACK_MANIFEST_PATH = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), "static", "manifest.json")
app.config['WEBPACK_ASSETS_URL'] = WEBPACK_ASSETS_URL = "/static/"

webpack = Webpack()
webpack.init_app(app)

DEBUG = app.config['DEBUG']
EPUB_PATH = app.config['EPUB_PATH']
if app.config['USER_AGENT'] == '':
    app.config['USER_AGENT'] = 'Mozilla/5.0 (X11; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0'
USER_AGENT = app.config['USER_AGENT']
TYPE_EPUB = app.config['TYPE_EPUB']

app.config['BABEL_DEFAULT_LOCALE'] = 'pt'


def get_resource_as_string(name, charset='utf-8'):
    with app.open_resource(os.path.join("static", "assets", webpack.asset_url_for(name).split('/')[2])) as f:
        return f.read().decode(charset)

app.jinja_env.globals['get_resource_as_string'] = get_resource_as_string

tryton = Tryton(app)
User = tryton.pool.get('res.user')
Articles = tryton.pool.get('epubfy.articles')
Domains = tryton.pool.get('epubfy.domains')
Files = tryton.pool.get('epubfy.files')

# create epub_path if not exists
try:
    os.makedirs(EPUB_PATH, exist_ok=True)  # python > 3
except TypeError:
    # os.makedirs(EPUB_PATH)  # not working in python 2.7.13
    assert os.path.isdir(EPUB_PATH)  # assert dir exists in python 2
except OSError as exc:
    if exc.errno == errno.EEXIST and os.path.isdir(EPUB_PATH):
        pass
    else:
        raise


@tryton.default_context
def default_context():
    return User.get_preferences(context_only=True)


@app.cli.command()
@tryton.transaction(readonly=True)
def printdb():
    """Print the database."""
    art = Articles.search([()])
    print('Articles: ' + str(len(art)))
    print('')
    for a in art:
        print('name: {}'.format(a.name))
        print('url: {}'.format(a.url))
        print('domain: {}'.format(a.domain.domain))
        print('files: {}'.format(', '.join(f.type.upper() for f in a.files)))
        print('')

    dom = Domains.search([()])
    print('\n--------------------\n')
    print('Domains: ' + str(len(dom)))
    print('')
    for d in dom:
        print('domain: {}'.format(d.domain))
        art = Articles.search([('domain', '=', d.domain)])
        print('articles: {}'.format(', '.join([a.name for a in art])))
        print('')

    files = Files.search([()])
    print('\n--------------------\n')
    print('Files: ' + str(len(files)))
    print('')
    for f in files:
        print('filename: {}'.format(f.filename))
        print('type: {}'.format(f.type))
        print('article: {}'.format(f.article.name))
        print('')

import views
