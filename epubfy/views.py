from __future__ import print_function

import os

from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import send_file
from flask import send_from_directory
from flask import session
from flask import url_for
from flask_babel import gettext

from .epubfy import DEBUG
from .epubfy import EPUB_PATH
from .epubfy import app
from .epubfy import babel
from .epubfy import tryton
from .epubfy import Articles
from .epubfy import Files
from .helper import parse_page


@app.url_value_preprocessor
def pull_lang_code(endpoint, values):
    if values and 'lang_code' in values:
        g.lang_code = values.pop('lang_code', None)
        if g.lang_code not in app.config['LANGUAGES'].keys():
            g.lang_code = request.accept_languages.best_match(
                app.config['LANGUAGES'].keys())
    else:
        if 'lang_code' in session:
            g.lang_code = session['lang_code']
        else:
            g.lang_code = request.accept_languages.best_match(
                app.config['LANGUAGES'].keys())


@app.url_defaults
def add_language_code(endpoint, values):
    if 'lang_code' in values or not g.lang_code:
        return
    if app.url_map.is_endpoint_expecting(endpoint, 'lang_code'):
        values['lang_code'] = g.lang_code


@babel.localeselector
def get_locale():
    return g.lang_code


@app.route('/')
def indexn():
    return redirect(url_for('index', lang_code=g.lang_code))


@app.route('/<lang_code>/')
@tryton.transaction(readonly=True)
def index(lang_code=None):
    session['lang_code'] = g.lang_code
    articles = Articles.search([])
    n_articles = len(articles)
    return render_template("index.html", n_articles=n_articles, articles=articles)


@app.route('/create', methods=['POST'])
def create():
    url = request.form['url']

    # form URL, if necessary
    if 'http' not in url[:7]:
        url = 'http://' + url

    result = parse_page(url)
    if 'errors' in result:
        if DEBUG:
            print('[DEBUG] view.create.error: ', result)
        for error in result['errors']:
            flash(error, 'warning')
    elif 'new_article' in result:
        if DEBUG:
            print('[DEBUG] view.create.ok: ', result)
        flash(gettext("Epub created!"), 'success')
    else:
        if DEBUG:
            print('[DEBUG] view.create.ops: ', result)
        flash(gettext("Server error. Please, contact us."), 'danger')
    return redirect(url_for('index'))


@app.route('/<ebook_type>/<filename>')
@tryton.transaction(readonly=True)
def get_ebook(ebook_type, filename):
    filepath = os.path.join(app.config['EPUB_PATH'], filename)
    if os.path.isfile(filepath):
        files = Files.search([('filename', '=', filename)])
        name = str(files[0].article.name) + '.' + str(files[0].type)
        return send_file(
            os.path.join(EPUB_PATH, filename),
            attachment_filename=name,
            as_attachment=True,
            mimetype='application/epub+zip'
        )
    else:
        if DEBUG:
            print('[DEBUG] view.epub.send: File not found. ' + filename + ': ' + filename)
        flash(gettext("File not found. Contact us if it should exist."), 'danger')
    return redirect(url_for('index'))


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static', 'images'),
                               'favicon.png', mimetype='img/png')
