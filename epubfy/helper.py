from __future__ import print_function

import os
import re
import uuid
from collections import defaultdict
from unicodedata import normalize

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

import microdata
import requests
from bs4 import BeautifulSoup
from ebooklib import epub
from requests import RequestException
from requests.packages.urllib3.exceptions import HTTPError, ConnectionError, TimeoutError

from .epubfy import DEBUG, EPUB_PATH, USER_AGENT, TYPE_EPUB
from .epubfy import tryton
from .epubfy import Articles
from .epubfy import Domains
from .epubfy import Files


def add_to_db(url, title, filename, file_type):
    result = defaultdict(list)

    hostname = urlparse(url).hostname
    a_name = re.sub(r"[\W]", "", title.strip().replace(" ", "_"))
    new_domain = Domains.search([('domain', '=', hostname)])
    # try:
    # if not new_domain:
    #     new_domain = Domains.create([{u'domain': hostname}])
    #     new_domain = new_domain[0]

    new_article = Articles.create([{
        u'url': url,
        u'name': a_name,
        # u'domain': hostname,
    }])

    new_file = Files.create([{
        u'filename': filename,
        u'type': file_type,
        u'article': new_article[0]
    }])
    # except:
    #     if DEBUG:
    #         print('[DEBUG] Unable to add entry to database.')
    #     result['errors'].append("Unable to add entry to database.")

    result['new_domain'] = new_domain
    result['new_article'] = new_article
    result['file'] = new_file

    return result


def clean(soup):
    remove_tags = [
        'header',
        'footer',
        'section',
        'script',
        'form',
        'input',
        'style',
        'iframe'
    ]

    attr_keep = [
        'img',
        'a',
        'link'
    ]

    for tag in remove_tags:
        for t in soup.find_all(tag):
            t.decompose()
    soup.attrs = None
    for tag in soup.findAll(True):
        if tag.name not in attr_keep:
            tag.attrs = None

    if soup.find('p') is None:
        if soup.find_all('b'):
            soup = re.sub(r"<b>|<b/>", "<p>", str(soup))

    return soup


def fetch_url(url):
    result = defaultdict(list)

    try:
        r = requests.get(url, verify=False, timeout=5, headers={'User-Agent': USER_AGENT})
    except ConnectionError:
        if DEBUG:
            print('[DEBUG] ConnectionError')
        result['errors'].append(
            "Connection error. Please try again or contact us."
        )
        return result
    except TimeoutError:  # Giving an error
        if DEBUG:
            print('[DEBUG] Timeout')
            result['errors'].append(
                "There website did not answer. Please try again or contact us."
            )
        return result
    except HTTPError:
        if DEBUG:
            print('[DEBUG] HTTPError')
        result['errors'].append(
            "There was an error in the server. Please try again or contact us."
        )
        return result
    except RequestException:
        if DEBUG:
            print('[DEBUG] RequestException')
        result['errors'].append(
            "Could not fetch website. Please try again or contact us."
        )
        return result

    # Try to guess how to encode/decode/parse chars
    try:
        # encode-decode trick to fix some latin chars not properly parsed
        r = r.text.encode('latin1').decode('utf-8')
    except UnicodeEncodeError:
        if DEBUG:
            print('[DEBUG] Unicode encode error.')
        # normalize trick to fix some non-latin chars not properly parsed
        r = normalize('NFKD', r.text).encode('latin1', 'ignore')
    except UnicodeDecodeError:
        if DEBUG:
            print('[DEBUG] Unicode decode error.')
        r = r.text.encode()
    finally:
        result['html'] = r
    return result


def opengraph(soup):
    ogs = soup.html.head.findAll(property=re.compile(r'^og'))
    og = dict()
    for tag in ogs:
        if tag.has_attr('content'):
            og[tag['property'][3:]] = tag['content']
    return og if len(og) else None


def get_metadata(soup, html):
    # metadata we want in the book
    metadata = {
        'title': '',
        'author': '',
        'uid': '',
        'language': '',
        'description': '',
        'publisher': '',
        'date': '',
        'type': ''
    }

    # Get microdata
    m = microdata.get_items(html)
    if len(m):
        m = m[0].json_dict()
        if 'name' in m['properties']:
            metadata['title'] = m['properties']['name'][0].strip()
        if 'description' in m['properties']:
            metadata['description'] = m['properties']['description'][0].strip()
        if 'dateModified' in m['properties']:
            metadata['date'] = m['properties']['dateModified'][0].strip()
        if 'dateCreated' in m['properties']:
            metadata['date'] = m['properties']['dateCreated'][0].strip()
        if 'datePublished' in m['properties']:
            metadata['date'] = m['properties']['datePublished'][0].strip()
        if 'type' in m['properties']:
            metadata['type'] = m['properties']['type'][0].strip()
        if 'author' in m['properties']:
            metadata['author'] = m['properties']['author'][0].strip()
        if 'creator' in m['properties']:
            metadata['author'] = m['properties']['creator'][0].strip()
        if 'inLanguage' in m['properties']:
            metadata['language'] = m['properties']['inLanguage'][0].strip()
        if 'language' in m['properties']:
            metadata['language'] = m['properties']['language'][0].strip()
        if 'producer' in m['properties']:
            metadata['publisher'] = m['properties']['producer'][0].strip()
        if 'publisher' in m['properties']:
            metadata['publisher'] = m['properties']['publisher'][0].strip()
        if 'publisher' in m['properties']:
            metadata['publisher'] = m['properties']['publisher'][0].strip()
    else:
        # m = None
        if DEBUG:
            print('[DEBUG] Mircodata: No microdata found.')

    # Get OGP
    og = opengraph(soup)
    if og is None:
        if DEBUG:
            print('[DEBUG] OGP: No OpenGraph data found.')
    else:
        if 'title' in og and metadata['title'] == '':
            metadata['title'] = og['title']
        if 'description' in og and metadata['description'] == '':
            metadata['description'] = og['description']
        if 'site_name' in og and metadata['publisher'] == '':
            metadata['publisher'] = og['site_name']
        if 'type' in og and metadata['type'] == '':
            metadata['type'] = og['type']

    # TODO: Gather, microformats, JSON-LD - Is it really needed? So far microdata is doing fine.
    # TODO: Come back to 'Article' tag and other common ones if semantic markup not found

    # We need an author
    if metadata['author'] == '':
        metadata['author'] = metadata['publisher']
    return metadata


def extract_title(article, url, soup):
    # title from article
    if 'header' in article:
        title = article.header.find_all(["title", "h1"])
        title = '\n'.join([str(l.text) for l in title])
    else:
        title = article.find_all(["title", "h1", "h2"])
        title = '\n'.join([str(l.text) for l in title])

    # title from url
    if title is None:
        title = url.split('/')[-1]
        title = os.path.splitext(title)[0]
        title = re.sub(r"[-_]", " ", title)
        title = re.sub(r"[\W]", "", title)

    # Title from page title
    if title is None or len(title) < 5:
        title = soup.find('title').string.strip()

    return title


def extract_article(soup):
    # ideas taken from https://github.com/macborow/repub/blob/master/repub.py
    article = ''
    if DEBUG:
        print('[DEBUG] Looking for Articles in DIV tags.')
    # some blogs define section id="content", let's try to be smart about it
    content_selectors = [
        {"name": "article", "role": "main"},
        {"name": "section", "class": "main_cont"},
        {"name": "div", "id": "sn-content"},
        {"name": "div", "id": "content"},
        {"name": "div", "data-zone": "contentSection"},
        {"name": "div", "class": "contentbox"},
        {"name": "section", "id": "content"},
        {"name": "div", "id": "maincontent"},
        {"name": "div", "id": "main-content"},
        {"name": "div", "class": "main-content"},
        {"name": "div", "class": "single-archive"},
        {"name": "div", "class": "blogcontent"},
        {"name": "div", "class": "post"},
        {"name": "div", "class": "content"},
        {"name": "div", "id": "post"},
        {"name": "div", "class": "hentry"},
        {"name": "div", "class": "post-body"},
        {"name": "div", "class": "post-single-body"},
        {"name": "div", "itemprop": "articleBody"},
        {"name": "div", "class": "h-entry"},
        {"name": "div", "class": "article-single-container"},
        {"name": "div", "id": "story-content"},
    ]
    content_candidates = []
    for selector in content_selectors:
        content_sections = soup.find_all(**selector)
        if content_sections:
            content_candidates.extend(content_sections)

    # select the largest section from the ones filtered out above
    if DEBUG:
        print('[DEBUG] Found', len(content_candidates), 'probable articles')
    if len(content_candidates):
        for contentCandidate in content_candidates:
            if len(str(contentCandidate)) > len(article):
                article = contentCandidate
    return article if article != '' else None


def create_epub(article, metadata):
    file_name = str(uuid.uuid4())
    path = os.path.join(EPUB_PATH, file_name)

    book = epub.EpubBook()
    # set metadata
    book.set_identifier(file_name)
    book.set_title(metadata['title'])
    book.set_language(metadata['language'] or 'en')
    book.add_author(metadata['author'])

    # add CSS file
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'resources', 'epub.css'), 'rb') as f:
        css = f.read()
    epub_css = epub.EpubItem(uid="styles", file_name="epub.css", media_type="text/css", content=css)
    book.add_item(epub_css)

    # add font file only if there is a tag that uses it
    if '<pre>' in article or '<code>' in article or '<tt' in article:
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'resources', 'Inconsolata.otf'), 'rb') as f:
            css = f.read()
        epub_font = epub.EpubItem(uid="inconsolata",
                                  file_name="Inconsolata.otf",
                                  media_type="application/font-sfnt",
                                  content=css)
        book.add_item(epub_font)

    c1 = epub.EpubHtml(title=metadata['title'], file_name='article_01.xhtml', lang=metadata['language'] or 'en')
    c1.content = article
    c1.add_item(epub_css)

    book.add_item(c1)
    book.toc = (c1,)
    book.add_item(epub.EpubNcx())
    book.add_item(epub.EpubNav())
    book.spine = [c1]

    epub.write_epub(path, book, {})

    return file_name


@tryton.transaction()
def parse_page(url):
    result = defaultdict(list)

    article_q = Articles.search([('url', '=', url.strip())])
    if article_q:
        if DEBUG:
            print('[DEBUG] Article Exists.')
        result['article_id'] = article_q[1]
        result['errors'].extend(['Article already exists.'])
        return result

    html = fetch_url(url)
    if 'errors' in html:  # fetching did not go well
        if DEBUG:
            print('[DEBUG] parse_page: Request failed')
        return html
    html = html['html']

    # TODO: Limit range of BeautifulSoup parsing
    soup = BeautifulSoup(html, "lxml")

    metadata = get_metadata(soup, html)

    # TODO: fetch images
    # TODO: add many articles or all articles on page

    article = soup.find('article')
    if article is None:
        if DEBUG:
            print('[DEBUG] parse_page: No <article> found.')
        article = extract_article(soup.body)
    if article is None:
        if DEBUG:
            print('[DEBUG] parse_page: No Articles found.')
        result['errors'].append(
            "No articles found on page. If you think this is a mistake, please, write us."
        )
        return result

    if metadata['title'] is None or metadata['title'] == '':
        metadata['title'] = extract_title(article, url, soup)

    article = clean(article)

    if 'h1' not in article and 'h2' not in article and 'title' not in article:
        article = '<h1>' + str(metadata['title']) + '</h1>' + str(article)

    # Build file
    # file_name = str(uuid.uuid3(uuid.NAMESPACE_URL, url))
    file_name = create_epub(article=article, metadata=metadata)

    # Save to DB
    add_db = add_to_db(url, metadata['title'], file_name, TYPE_EPUB)
    if 'errors' in add_db:
        result['errors'].extend(add_db['errors'])
    if 'new_article' in add_db:
        result['new_article'] = add_db['new_article']
    if 'new_domain' in add_db:
        result['new_domain'] = add_db['new_domain']

    if DEBUG:
        print('[DEBUG] parse_page result:', result)

    return result
