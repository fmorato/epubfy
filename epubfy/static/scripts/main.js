(function () {

    'use strict';

    angular.module('Epubfy', [])


    .controller('EpubfyController', ['$scope', '$log', '$timeout', '$http', function($scope, $log, $timeout, $http) {
        $scope.submitButtonText = "Create EPUB";
        $scope.loading = false;
        $scope.urlError = false;
        $scope.errors = [];
        $scope.books = [];
        $scope.warning = false;
        $scope.warns = [];
        $log.log("started");

        $scope.createEpub = function() {
            $log.log("button");

            function getEpub(jobID) {

                var timeout = "";

                var poller = function() {
                    // fire another request
                    $http.get('/ready/'+jobID).
                    success(function(data, status, headers, config) {
                        if(status === 202) {
                        //$log.log(data, status);
                        //$log.log(data.errors);
                        var error;
                        if (data.errors) {
                        for (error in data.errors){
                        $scope.errors.push(data.errors[error]);
                        $log.log(data.errors[error]);
                        }
                        $scope.loading = false;
                        $scope.urlError = true;
                        $scope.submitButtonText = "Create EPUB";
                        $timeout.cancel(timeout);
                        return false;
                    }
                    } else if (status === 200){
                        //$log.log(data);
                        $scope.loading = false;
                        $scope.submitButtonText = "Create EPUB";
                        var book;
                        if ($scope.books.length){
                            for (book in $scope.books){
                                $log.log($scope.books[book]);
                                if ($scope.books[book].path == data.path) {
                                $scope.warning = true;
                                $scope.warns.push("Book already fetched.");
                                break;
                            }
                            $scope.books.push(data);
                        }
                    } else  {
                        $log.log("first");
                        $scope.books.push(data);
                    }
                    $log.log($scope.books);
                    $timeout.cancel(timeout);
                    return false;
                    }
                    // continue to call the poller() function every 2 seconds
                    // until the timeout is cancelled
                    timeout = $timeout(poller, 2000);
                }).
                error(function(error) {
                    $log.log(error);
                    $scope.errors.push('Could not contact the server. Is your connection working? If the problem persists, please contact us.');
                    $scope.urlError = true;
                    $scope.loading = false;
                    $scope.submitButtonText = "Create EPUB";
                });
            };

            poller();
            }
            //TODO: Handle errors sent by server
            //TODO: Don't make duplicates, keep track of links.'
            // get the URL from the input
            var userInput = $scope.input_url;
            $log.log(userInput);
            $scope.warns = [];
            $scope.warning = false;
            // fire the API request
            $scope.errors = [];
            $scope.urlError = false;
            $http.post('/create', {"url": userInput}).

            success(function(results) {
                //$log.log(results);
                getEpub(results);
                $scope.loading = true;
                $scope.submitButtonText = "Loading...";
            }).

            error(function(error) {
                $log.log(error);
                $scope.errors.push('Could not contact the server. Is your connection working? If the problem persists, please contact us.');
                $scope.urlError = true;
                $scope.loading = false;
                $scope.submitButtonText = "Create EPUB";
            });

            };
        }
    ])
}());
