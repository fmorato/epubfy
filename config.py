# coding=utf-8
import os


class Config(object):
    DEBUG = False
    DEVELOPMENT = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = os.environ.get('SECRET_KEY', '')
    SEND_FILE_MAX_AGE_DEFAULT = 60 * 60 * 24 * 30  # seconds * minutes * hours * days
    USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:44.0)  Gecko/20100101 Firefox/44.0'
    TYPE_EPUB = 'epub'
    TYPE_KINDLE = 'kindle'
    EBOOK_PATH = os.environ.get('EBOOK_PATH', os.path.join(os.path.dirname(os.path.abspath(__file__)), "books"))
    EPUB_PATH = os.path.join(EBOOK_PATH, TYPE_EPUB)
    KINDLE_PATH = os.path.join(EBOOK_PATH, TYPE_KINDLE)
    LANGUAGES = {
        'en': 'English',
        'pt': 'Português'
    }
    TRYTON_DATABASE = os.environ.get('TRYTON_DATABASE', 'epubfy')
    TRYTON_USER = os.environ.get('TRYTON_USER', 0)
    TRYTON_CONFIG = os.environ.get('TRYTON_CONFIG', '/opt/tryton.conf')


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
