from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='epubfy',
    version='1.1.1',
    description='Converts bnlog articles to ebooks.',
    long_description=readme(),
    keywords='',
    url='https://epubfy.herokuapp.com',
    author='Felipe Morato',
    author_email='me@fmorato.com',
    license='',
    packages=['epubfy'],
    include_package_data=True,
    install_requires=[
        'bs4'
        , 'EbookLib'
        , 'flask'
        , 'flask-babel'
        , 'flask-compress'
        , 'flask-HTMLmin'
        , 'flask-tryton'
        , 'flask-webpack'
        , 'microdata'
        , 'psycopg2'
        , 'requests'
    ],
    tests_require=[
        'pytest'
        , 'pytest-cov'
    ],
)
