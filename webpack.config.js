var path = require('path');
var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var ManifestRevisionPlugin = require('manifest-revision-webpack-plugin');
var purify = require("purifycss-webpack-plugin");
var WebpackCleanupPlugin = require('webpack-cleanup-plugin');
var webpack = require('webpack');

var rootAssetPath = './epubfy/static';


module.exports = {
  entry: {
    app_css: ['./epubfy/static/styles/main.scss']
  },
  output: {
    path: path.resolve(__dirname, 'epubfy/static/assets'),
    filename: '[name].[hash].js'
  },
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract([
          'css',
          'postcss',
          'sass'
        ])
      },
      {
        test: /\.(eot|otf|png|svg)(\?v=[0-9.]+)?$/,
        loader: 'file?name=[name].[hash].[ext]',
        include: [
          // path.resolve(__dirname, 'node_modules'),
          path.resolve(__dirname, 'epubfy/static/font')
        ]
      },
      {
        test: /\.(ttf|woff|woff2)(\?[0-9.]+|\?v=[0-9.]+)?$/,
        loader: 'base64-font-loader'
      },
    ]
  },
  plugins: [
    new WebpackCleanupPlugin(),
    new ExtractTextPlugin('[name].[chunkhash].css'),
    new purify({
      basePath: __dirname,
      resolveExtensions: ['.html'],
      paths: [
        "./**/templates/**/*.html"
      ],
      purifyOptions: {
        minify: true,
        output: false,
        info: true,
        whitelist: [
        ]
      }
    }),
    new ManifestRevisionPlugin(path.join('epubfy', 'static', 'manifest.json'), {
      rootAssetPath: './epubfy/static/assets',
      ignorePaths: ['manifest.json', '/assets']
    })
  ],
  postcss: function () {
    return [
      autoprefixer({
        browsers: [
          "Android 2.3",
          "Android >= 4",
          "Chrome >= 20",
          "Firefox >= 24",
          "Explorer >= 8",
          "iOS >= 6",
          "Opera >= 12",
          "Safari >= 6"
        ]
      })
    ];
  }
};
